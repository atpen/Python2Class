import random

# Create a function called game() to run the main game
def game():
    attempts = 0 

    # Create a variable called HANGMAN that hold a list of all the images using ASCII graphics for the hangman. The amount of images you create will based on how many turns the payer will have.
    #Use multi line string format
    HANGMAN = [
"""
-----
|   |
|
|
|
|
|
|
|
--------
""",
"""
-----
|   |
|   o
|
|
|
|
|
|
--------
""",
"""
-----
|   |
|   o
|  /|\\
|   
|
|
|
|
--------
""",
"""
-----
|   |
|   o
|  /|\\
|  / \\
|
|
|
|
--------
""",
"""                       ______
                    .-"      "-.
                   /            \\
       _          |              |          _
      ( \         |,  .-.  .-.  ,|         / )
       > "=._     | )(__/  \__)( |     _.=" <
      (_/"=._"=._ |/     /\     \| _.="_.="\_)
             "=._ (_     ^^     _)"_.="
                 "=\__|IIIIII|__/="
                _.="| \IIIIII/ |"=._
      _     _.="_.="\          /"=._"=._     _
     ( \_.="_.="     `--------`     "=._"=._/ )
      > _.="                            "=._ <
     (_/                                   \_)
"""
]


    # create another list called words that will hold the string of all the words or phrases you want the other person to guess. You should have at least 10
    words = ["test", "hello", "banana", "apple", "chicken", "pineapple", "blueberry", "strawberries", "chthonic", "squush", "zugzwang", "ytterbium", "randkluft", "yclept", "diphthong", "squdgy", "perfluoroalkyl", "xenophile"] #add words here

    gameloop = True
    # make a variabe called gameloop and set it to True

    # while gameloop to start actual loop of the game
    while gameloop == True:
        # create a new variable called chosen_word that holds random.choice(words)
        chosen_word = random.choice(words)
        # create a new variable called attempts that will hold the number of turns
        turnNum = 1
        # create a new variable called joined_word that holds an empty string
        joined_word = ""
        # create 2 new variables called guessed_letters and word_guessed and set them equal to empty lists
        guessed_letters = []
        word_guessed = []
        # this is a for loop. for each letter in the chosen_word variable,it will append a "-" to the word guessed list
        for letter in chosen_word:
            word_guessed.append("-")


        #print your first HANGMAN picture using this code -> HANGMAN[attempts]

        print(HANGMAN[attempts])

        while attempts != 4 and "-" in word_guessed:
            # print to the user how many turns or attempts they have left
            print("You have "+ str(4 - attempts) + " attempts left.")
            # allows the chosen word in the word guessed list to print out and look normal
            joined_word = ''.join(word_guessed)
            print(joined_word)
            # create a guess variable that holds an input asking the player to guess a number
            guessed_letter = ""

            answer_check = True
            test2 = False
            while answer_check:
                guessed_letter = input("Guess a letter:").lower()
                # need to check to make sure that the guess variable is a letter and is not in guessed letters. If they they are, then the user needs to enter a different answer
                if guessed_letter in guessed_letters:
                    print("Enter a not already entered letter")
                    continue
                if len(guessed_letter) == 1:
                    answer_check = False
                else:
                    print("You need to enter a single letter")
                    continue
            #append the guess variable to the guessed letters list
                guessed_letters.append(guessed_letter)
            # a for loop leave alone dont change unless you chose a different name for your guess variable
            inWordTrue = False
            for letter in range(len(chosen_word)):
                if guessed_letter == chosen_word[letter]:
                    word_guessed[letter] = guessed_letter
                    inWordTrue = True
            # create an if statment that if the guess variable is not in chosen_word then increase attempts by 1 and print next hangman in list
            if inWordTrue == False:
                attempts += 1
                print(HANGMAN[attempts])
            else:
                print(HANGMAN[attempts])
        # an if/else statement to check if you won or lost please let the user know what the chosen word or phrase was
        if "-" not in word_guessed:
            print("You have guessed the word! It was " + chosen_word + ".")
        else:
            print("You have lost. The word was: " + chosen_word)
        # create a user input that asks the user if they want to play again. if the user does not turn gameloop false.
        checkContinue = input("Play again? (y/n): ")
        if (len(checkContinue) > 0):
            checkContinue = checkContinue[0]
        if checkContinue == "y":
            print("New game is starting.")
            attempts = 0 
            print("New game.")
        else:
            gameloop = False
            print(str(checkContinue[0] == "y"))
            print("Okay, bye.")








#run the game dont erase unless you dont want the game function to run.
game()
