from random import randint

board = []
playerBoard =[]

for x in range(5):
  playerBoard.append(["O"] * 5)
for x in range(5):
  board.append(["O"] * 5)

def print_board(board):
  for row in board:
    print( " ".join(row))

def random_row(board):
  return randint(0, len(board) - 1)

def random_col(board):
  return randint(0, len(board[0]) - 1)

ship_row = int(random_row(board))
ship_col = int(random_col(board))
playerShip_row = int(input("Your Ship Row(1 to 5): "))-1
playerShip_col = int(input("Your Ship Col(1 to 5): "))-1
turn = 0 
computerGuess_row = 666
computerGuess_col = 666

# Everything from here on should go in your for loop!
# Be sure to indent four spaces!

while True:
    print("Turn " + str(turn+1))
    print("Computer Ship Board")
    print_board(board)
    print("My Ship Board")
    print_board(playerBoard)
    guess_row = int(input("Guess Row: "))-1
    guess_col = int(input("Guess Col: "))-1

    if guess_row == ship_row and guess_col == ship_col:
        print ("Congratulations! You sunk the computer's battleship!")
        break
    else:
        if (guess_row < 0 or guess_row > 4) or (guess_col < 0 or guess_col > 4):
            print ("Oops, that's not even in the ocean.")
        elif(board[guess_row][guess_col] == "X"):
            print ("You guessed that one already.")
        else:
            print ("You missed my battleship!")
            board[guess_row][guess_col] = "X"
            if turn == 3:
                print ("Game Over")

    computerGuess_row = randint(0,4)
    computerGuess_col = randint(0,4)
    while board[computerGuess_row][computerGuess_col] == "X":
      computerGuess_row = randint(0,4)
      computerGuess_col = randint(0,4)

    if guess_row == ship_row and guess_col == ship_col:
        print ("The computer sunk your battleship.")
        break
    else:
        print ("The computer missed your battleship")
        playerBoard[computerGuess_row][computerGuess_col] = "X"
        #print("added x to " + str(computerGuess_row) + " " + str(computerGuess_col)) used to debugg code
    turn+=1
    
