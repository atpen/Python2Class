#Copyright 2021 Matt Peng
#Do not distribute without permision
#All rights reserved
from random import randint

board = []
playerBoard =[]
computerShips = {}
playerShips = {}

class Color:
    Red = "\033[31m"
    White = "\033[37m"

for x in range(6):
  playerBoard.append(["O"] * 6)
for x in range(6):
  board.append(["O"] * 6)

def print_board(board):
  for row in board:
    print( " ".join(row))

def random_row(board):
  return randint(0, len(board) - 1)

def random_col(board):
  return randint(0, len(board[0]) - 1)

numComputerShips = input("How many ships should the computer have? ")
for i in range(int(numComputerShips)):
  #print("added " + str(i) + " ship") was debug code
  computerShip_row = int(random_row(board))
  computerShip_col = int(random_col(board))
  #print("ship col/row generated " + str(i) + " " + str(computerShip_row) + " " + str(computerShip_col)) was debug code
  while computerShip_row in computerShips and computerShip_col == computerShips[computerShip_row]:
    computerShip_row = int(random_row(board))
    computerShip_col = int(random_col(board))
    #print("ship col/row regenerated " + str(i) + " " + str(computerShip_row) + " " + str(computerShip_col)) was debug code
  computerShips[computerShip_row] = computerShip_col
  #print("added ship to dict " + str(computerShips)) was debug code
numComputerShips = int(input("How many ships should you have? "))
for i in range(numComputerShips):
  playerShip_row = int(input("Your " + str(i+1) + " " + "Ship Row(1 to 5): "))-1
  playerShip_col = int(input("Your " + str(i+1) + " " + "Ship Col(1 to 5): "))-1
  playerShips[playerShip_row] = playerShip_col

turn = 0 
computerGuess_row = 666
computerGuess_col = 666

# Everything from here on should go in your for loop!
# Be sure to indent four spaces!

while True:
    print("Turn " + str(turn+1))
    print("Computer Ship Board")
    print_board(board)
    print("My Ship Board")
    print_board(playerBoard)
    #print(computerShips) was debug code
    guess_row = int(input("Guess Row: "))-1
    guess_col = int(input("Guess Col: "))-1

    if guess_row in computerShips and guess_col == computerShips[guess_row]:
        print ("Congratulations! You sunk one of the computer's battleship!")
        board[guess_row][guess_col] = Color.Red + "X" + Color.White
        del computerShips[guess_row]
        if not computerShips:
          print("You have sunk all of the computer's battleships! Great Job!")
          break
    else:
        if (guess_row < -1 or guess_row > 6) or (guess_col < -1 or guess_col > 6):
            print ("Oops, that's not even in the ocean.")
        elif(board[guess_row][guess_col] == "X"):
            print ("You guessed that one already.")
        else:
            print ("You missed the computer's battleship!")
            board[guess_row][guess_col] = Color.Red + "X" + Color.White
            if turn == 3:
                print ("Game Over")

    computerGuess_row = randint(0,5)
    computerGuess_col = randint(0,5)
    while board[computerGuess_row][computerGuess_col] == "X":
        computerGuess_row = randint(0,5)
        computerGuess_col = randint(0,5)

    if guess_row in playerShips and guess_col == playerShips[guess_row]:
        print ("The computer sunk your battleship.")
        break
    else:
        print ("The computer missed your battleship")
        playerBoard[computerGuess_row][computerGuess_col] = Color.Red + "X" + Color.White
        #print("added x to " + str(computerGuess_row) + " " + str(computerGuess_col)) used to debugg code
    turn+=1