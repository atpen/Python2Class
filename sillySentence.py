     # Matt Peng
     # 11/30/2021
     # This is a program that generates silly sentences 

import random 
names = []
verbs = []
nouns = []
adjectives = []
continue_ = True

def pick(words):
     num_words = len(words)
     num_picked = random.randint(0, num_words - 1)
     word_picked = words[num_picked]
     return word_picked 
def addRandom():
     if len(names) == 0 or len(verbs) == 0 or len(nouns) == 0 or len(adjectives) == 0:
          names.append(str(input("Enter a name to be add to the list of possible names:")))
          verbs.append(str(input("Enter a verb to be add to the list of possible verbs:")))
          nouns.append(str(input("Enter a noun to be add to the list of possible nouns:")))
          adjectives.append(str(input("Enter an adjective to be add to the list of possible adjectives:")))
          return 1
     choice = random.randint(0, 3)
     if (choice == 0):
          names.append(str(input("Enter a name to be add to the list of possible names:")))
     if (choice == 1):
          verbs.append(str(input("Enter a verb to be add to the list of possible verbs:")))
     if (choice == 2):
          nouns.append(str(input("Enter a noun to be add to the list of possible nouns:")))
     if (choice ==3):
          adjectives.append(str(input("Enter an adjective to be add to the list of possible adjectives:")))
while(continue_ == True):
     if len(names) == 0 or len(verbs) == 0 or len(nouns) == 0 or len(adjectives) == 0:
          if input("Would you like to use the default word list?(Y/N)").lower() == "y":
               nouns = "Answer,Helicopter,Planet,Apple,Helmet,Plastic,Army,Holiday".split(",")
               verbs = "Exit,Imitate,Invent,Jump,Laugh,Lie,Listen,Paint,Plan,Play,Read,Replace".split(",")
               names = "Liam,Olivia,Emma,Noah,Ava,Elijah,Oliver,Sophia,Amelia".split(",")
               adjectives = "eager,easy,elated,elegant,embarrassed,enchanting,encouraging,energetic,enthusiastic,envious,evil,excited,expensive,exuberant,fair,faithful,famous".split(",")
     addRandom()
     for i in range(0, int(input("How many sentences do you want? "))):
          print (pick(names) + " " + pick(verbs) + " " + pick(adjectives) + " " + pick(nouns) + "." + "\n") 
     if(input("Type y if you want to continue, n otherwise:") == "y"):
          continue_ = True
     else:
          continue_ = False
